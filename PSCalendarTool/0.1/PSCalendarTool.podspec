Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "PSCalendarTool"
s.summary = "RWPickFlavor lets a user select an ice cream flavor."
s.requires_arc = true

# 2
s.version = "0.1"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Philipp Seitz" => "seitz.philipp@outlook.com" }

s.homepage = "https://bitbucket.org/ptothep"

s.source = { :git => "https://bitbucket.org/ptothep/pscalendartool.git", :tag => "#{s.version}"}



# 8
s.source_files = "PSCalendarTool/**/*.{h,m,swift}"
s.framework = "Foundation"
end